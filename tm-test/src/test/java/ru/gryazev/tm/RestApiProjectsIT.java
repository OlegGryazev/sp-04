package ru.gryazev.tm;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import ru.gryazev.tm.entity.Project;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RestApiProjectsIT {

    private static Project project = new Project();

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost/projectmanager-server";
        RestAssured.port = 8080;
    }

    @Test
    public void testAddGetDeleteProjectOk() {
        project.setName("assured-test-project");
        given().contentType(ContentType.JSON).body(project)
                .when().post("/projects/merge")
                .then().statusCode(HttpStatus.OK.value());

        given().contentType(ContentType.JSON)
                .when().get("/projects/" + project.getId())
                .then().body("name", equalTo("assured-test-project"));

        given().contentType(ContentType.JSON)
                .when().delete("/projects/delete/" + project.getId())
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void testAddProject() {
        final String responseBefore = get("/projects").asString();
        final int projectsCountBefore = JsonPath.from(responseBefore).getList("").size();
        given().contentType(ContentType.JSON).body(project)
                .when().post("/projects/merge")
                .then().statusCode(HttpStatus.OK.value());
        final String responseAfter = get("/projects").asString();
        final int projectsCountAfter = JsonPath.from(responseAfter).getList("").size();
        assertEquals(projectsCountAfter, projectsCountBefore + 1);
    }

    @Test
    public void testDeleteProjectNotFound() {
        given().contentType(ContentType.JSON)
                .when().delete("/projects/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Test
    public void testViewProjectNotFound() {
        final String response = get("/projects/test-assured-id").asString();
        assertTrue(response.isEmpty());
    }

    @Test
    public void testMethodIsNotAllowed() {
        given().contentType(ContentType.JSON)
                .when().delete("/projects/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().post("/projects")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().post("/projects/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().get("/projects/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().delete("/projects/add" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
    }

}
