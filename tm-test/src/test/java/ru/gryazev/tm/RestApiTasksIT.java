package ru.gryazev.tm;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RestApiTasksIT {

    private static Project project = new Project();

    private static Task task = new Task();

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost/projectmanager-server";
        RestAssured.port = 8080;
    }

    @Test
    public void testAddGetDeleteTaskOk() {
        task.setName("assured-test-task");
        task.setProjectId(project.getId());
        given().contentType(ContentType.JSON).body(task)
                .when().post("/tasks/merge")
                .then().statusCode(HttpStatus.OK.value());

        given().contentType(ContentType.JSON)
                .when().get("/tasks/" + task.getId())
                .then().body("name", equalTo("assured-test-task"));

        given().contentType(ContentType.JSON)
                .when().delete("/tasks/delete/" + task.getId())
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void testAddTask() {
        final String responseBefore = get("/tasks").asString();
        final int tasksCountBefore = JsonPath.from(responseBefore).getList("").size();
        given().contentType(ContentType.JSON).body(project)
                .when().post("/tasks/merge")
                .then().statusCode(HttpStatus.OK.value());
        final String responseAfter = get("/tasks").asString();
        final int tasksCountAfter = JsonPath.from(responseAfter).getList("").size();
        assertEquals(tasksCountAfter, tasksCountBefore + 1);
    }

    @Test
    public void testDeleteTaskNotFound() {
        given().contentType(ContentType.JSON)
                .when().delete("/tasks/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Test
    public void testViewTaskNotFound() {
        final String response = get("/tasks/test-assured-id").asString();
        assertTrue(response.isEmpty());
    }

    @Test
    public void testMethodIsNotAllowed() {
        given().contentType(ContentType.JSON)
                .when().delete("/tasks/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().post("/tasks")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().post("/tasks/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().get("/tasks/delete/" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());

        given().contentType(ContentType.JSON)
                .when().delete("/tasks/add" + "test-assured-id")
                .then().statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
    }

}
