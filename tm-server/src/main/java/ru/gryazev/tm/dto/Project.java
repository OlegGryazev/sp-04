package ru.gryazev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.repository.ITaskRepository;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String details;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static ProjectEntity toProjectEntity(final Project project, ITaskRepository taskRepository) {
        if (project == null) return null;
        @NotNull final ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(project.getId());
        projectEntity.setName(project.getName());
        projectEntity.setDetails(project.getDetails());
        projectEntity.setTasks(taskRepository.findByProjectId(project.getId()));
        projectEntity.setDateFinish(project.getDateFinish());
        projectEntity.setDateStart(project.getDateStart());
        projectEntity.setStatus(project.getStatus());
        projectEntity.setCreateMillis(project.getCreateMillis());
        return projectEntity;
    }

}
