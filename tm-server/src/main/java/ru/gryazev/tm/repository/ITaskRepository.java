package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.List;

@Repository
public interface ITaskRepository extends CrudRepository<TaskEntity, String> {

    public List<TaskEntity> findByProjectId(@NotNull final String projectId);

}
