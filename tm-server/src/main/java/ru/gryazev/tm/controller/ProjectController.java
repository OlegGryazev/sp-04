package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectController {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    @Autowired
    public ProjectController(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @RequestMapping(value = "projects", method = RequestMethod.GET)
    public List<Project> getProjects() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projectRepository.findAll().forEach(o -> projects.add(ProjectEntity.toProjectDto(o)));
        return projects;
    }

    @DeleteMapping("projects/delete/{projectId}")
    public void removeProject(@PathVariable("projectId") final String projectId) {
        projectRepository.deleteById(projectId);
    }

    @RequestMapping(value = "projects/merge", method = RequestMethod.POST)
    public void mergeProject(@RequestBody final Project project) {
        @Nullable ProjectEntity projectEntity = Project.toProjectEntity(project, taskRepository);
        if (projectEntity == null) return;
        projectRepository.save(projectEntity);
    }

    @RequestMapping(value = "projects/{projectId}", method = RequestMethod.GET)
    public Project getTask(@PathVariable("projectId") final String projectId) {
        return ProjectEntity.toProjectDto(projectRepository.findById(projectId).orElse(null));
    }

}
