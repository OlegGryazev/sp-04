package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.repository.IProjectRepository;
import ru.gryazev.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskController {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    @Autowired
    public TaskController(
            final ITaskRepository taskRepository,
            final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @RequestMapping(value = "tasks", method = RequestMethod.GET)
    public List<Task> getProjects() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskRepository.findAll().forEach(o -> tasks.add(TaskEntity.toTaskDto(o)));
        return tasks;
    }

    @RequestMapping(value = "tasks/merge", method = RequestMethod.POST)
    public void addTask(@RequestBody final Task task) {
        @Nullable final TaskEntity taskEntity = Task.toTaskEntity(task, projectRepository);
        if (taskEntity == null) return;
        taskRepository.save(taskEntity);
    }

    @RequestMapping(value = "tasks/{taskId}", method = RequestMethod.GET)
    public Task getTask(@PathVariable("taskId") final String taskId) {
        return TaskEntity.toTaskDto(taskRepository.findById(taskId).orElse(null));
    }

    @DeleteMapping("tasks/delete/{taskId}")
    public void removeTask(@PathVariable("taskId") final String taskId) {
        taskRepository.deleteById(taskId);
    }

}
