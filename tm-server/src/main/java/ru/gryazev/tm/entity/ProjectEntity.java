package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProjectEntity extends AbstractCrudEntity {

    @Nullable
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "project", orphanRemoval = true)
    private List<TaskEntity> tasks;

    @Nullable
    @Column(name = "project_name")
    private String name;

    @Nullable
    private String details;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Column(name = "timestamp")
    private Long createMillis = new Date().getTime();

    @Nullable
    public static Project toProjectDto(final ProjectEntity projectEntity) {
        if (projectEntity == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectEntity.getId());
        project.setName(projectEntity.getName());
        project.setDetails(projectEntity.getDetails());
        project.setDateFinish(projectEntity.getDateFinish());
        project.setDateStart(projectEntity.getDateStart());
        project.setStatus(projectEntity.getStatus());
        project.setCreateMillis(projectEntity.getCreateMillis());
        return project;
    }

}
