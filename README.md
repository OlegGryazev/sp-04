#### Project Manager

###### Remote repository: 
https://gitlab.com/OlegGryazev/sp-04
###### Software requirements:
* JDK 8
* Apache Maven 3.6.3
* PostgreSQL 12
    
###### Technology stack:
* Maven
* Spring MVC
* Feign
* Spring Data JPA
* Hibernate
* REST-assured
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
     
###### Run:
    mvn tomcat7:run