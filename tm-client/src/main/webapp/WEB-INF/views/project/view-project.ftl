<#import "../parts/common.ftl" as c>
<@c.page>
  <div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">${project.getName()}</h5>
      <p class="card-text">Details about project: ${project.getDetails()}</p>
      <#if dateStart??>
        <p class="card-text">Project starts: ${dateStart}</p>
      </#if>
      <#if dateFinish??>
        <p class="card-text">Project finish: ${dateFinish}</p>
      </#if>
      <p class="card-text">Status of project: ${project.getStatus().displayName()}</p>
      <a href="projects" class="btn btn-primary mt-3">Go back</a>
    </div>
  </div>
</@c.page>