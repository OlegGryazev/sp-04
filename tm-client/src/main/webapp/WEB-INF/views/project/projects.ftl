<#import "../parts/common.ftl" as c>
<@c.page>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th>Project ID</th>
            <th>Project name</th>
            <th>Details</th>
            <th>VIEW</th>
            <th>EDIT</th>
            <th>REMOVE</th>
        </tr>
        </thead>
        <tbody>
        <#list projects as project>
            <tr>
                <td class="align-middle">${project.getId()}</td>
                <td class="align-middle">${project.getName()}</td>
                <td class="align-middle">${project.getDetails()}</td>
                <td class="align-middle">
                    <form method="get" action="view-project">
                        <input type="hidden" name="projectId" value="${project.getId()}" />
                        <button class="btn btn-link" type="submit">VIEW</button>
                    </form>
                </td>
                <td class="align-middle">
                    <form method="get" action="edit-project">
                        <input type="hidden" name="projectId" value="${project.getId()}" />
                        <button class="btn btn-link" type="submit">EDIT</button>
                    </form>
                </td>
                <td class="align-middle">
                    <form method="post" action="project-remove">
                        <input type="hidden" name="projectId" value="${project.getId()}" />
                        <button class="btn btn-link" type="submit">REMOVE</button>
                    </form>
                </td>
            </tr>
        </#list>
        </tbody>
    </table>
    <a href="add-project" class="btn btn-primary">Add project</a>
    <a href="projects" class="btn btn-secondary">Refresh</a>
</@c.page>