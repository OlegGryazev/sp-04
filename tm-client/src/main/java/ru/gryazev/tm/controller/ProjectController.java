package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.RestServiceApi;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.util.HashMap;
import java.util.Map;

@Controller
public class ProjectController {

    private final RestServiceApi restServiceApi;

    @Autowired
    public ProjectController(final RestServiceApi restServiceApi) {
        this.restServiceApi = restServiceApi;
    }

    @GetMapping("projects")
    public String getProjects(Map<String, Object> model) {
        model.put("projects", restServiceApi.getProjects());
        return "project/projects";
    }

    @GetMapping("add-project")
    public String addProject(Map<String, Object> model) {
        model.put("statuses", Status.values());
        return "project/add-project";
    }

    @PostMapping("project-remove")
    public String removeProject(
            @RequestParam("projectId") String projectId,
            Map<String, Object> model
    ) {
        restServiceApi.deleteProject(projectId);
        model.put("projects", restServiceApi.getProjects());
        return "redirect:/projects";
    }

    @PostMapping({"add-project", "edit-project"})
    public String addProject(
            @RequestParam(value = "projectId", required = false) String projectId,
            @RequestParam("name") String name,
            @RequestParam("details") String details,
            @RequestParam("dateStart") String dateStart,
            @RequestParam("dateFinish") String dateFinish,
            @RequestParam("status") String status,
            Map<String, Object> model
            ) {
        @NotNull final Project project = new Project();
        if (projectId != null) project.setId(projectId);
        project.setName(name);
        project.setDetails(details);
        project.setDateStart(DateUtils.formatStringToDate(dateStart));
        project.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        project.setStatus(Status.valueOf(status));
        restServiceApi.mergeProject(project);
        model.put("projects", restServiceApi.getProjects());
        return "redirect:/projects";
    }

    @GetMapping("edit-project")
    public String editProject(
            @RequestParam("projectId") String projectId,
            Map<String, Object> model
    ) {
        @Nullable final Project project = restServiceApi.getProject(projectId);
        if (project == null) return "redirect:projects";
        model.putAll(getModelByProject(project));
        return "project/edit-project";
    }

    @GetMapping("view-project")
    public String viewProject(
            @RequestParam("projectId") String projectId,
            Map<String, Object> model
    ) {
        @Nullable final Project project = restServiceApi.getProject(projectId);
        if (project == null) return "redirect:projects";
        model.putAll(getModelByProject(project));
        return "project/view-project";
    }

    private Map<String, Object> getModelByProject(final Project project) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(project.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(project.getDateFinish());
        model.put("project", project);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
