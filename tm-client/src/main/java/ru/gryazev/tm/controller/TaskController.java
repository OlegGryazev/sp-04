package ru.gryazev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.RestServiceApi;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.util.HashMap;
import java.util.Map;

@Controller
public class TaskController {

    private final RestServiceApi restServiceApi;

    @Autowired
    public TaskController(final RestServiceApi restServiceApi) {
        this.restServiceApi = restServiceApi;
    }

    @GetMapping("tasks")
    public String getTasks(Map<String, Object> model) {
        model.put("tasks", restServiceApi.getTasks());
        return "task/tasks";
    }

    @PostMapping({"add-task", "edit-task"})
    public String addTask(
            @RequestParam("linkedProjectId") String projectId,
            @RequestParam(value = "taskId", required = false) String taskId,
            @RequestParam("name") String name,
            @RequestParam("details") String details,
            @RequestParam("dateStart") String dateStart,
            @RequestParam("dateFinish") String dateFinish,
            @RequestParam("status") String status,
            Map<String, Object> model
    ) {
        @NotNull final Task task = new Task();
        if (taskId != null) task.setId(taskId);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDetails(details);
        task.setDateStart(DateUtils.formatStringToDate(dateStart));
        task.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        task.setStatus(Status.valueOf(status));
        restServiceApi.mergeTask(task);
        model.put("tasks", restServiceApi.getTasks());
        return "redirect:/tasks";
    }

    @GetMapping("add-task")
    public String addTask(Map<String, Object> model) {
        model.put("projects", restServiceApi.getProjects());
        model.put("statuses", Status.values());
        return "task/add-task";
    }

    @GetMapping("edit-task")
    public String editProject(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        @Nullable final Task task = restServiceApi.getTask(taskId);
        if (task == null) return "redirect:tasks";
        model.put("projects", restServiceApi.getProjects());
        model.putAll(getModelByTask(task));
        return "task/edit-task";
    }

    @PostMapping("task-remove")
    public String removeTask(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        restServiceApi.deleteTask(taskId);
        model.put("tasks", restServiceApi.getTasks());
        return "redirect:/tasks";
    }

    @GetMapping("view-task")
    public String viewTask(
            @RequestParam("taskId") String taskId,
            Map<String, Object> model
    ) {
        @Nullable final Task task = restServiceApi.getTask(taskId);
        if (task == null) return "redirect:tasks";
        model.putAll(getModelByTask(task));
        return "task/view-task";
    }

    private Map<String, Object> getModelByTask(final Task task) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(task.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(task.getDateFinish());
        model.put("task", task);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
