package ru.gryazev.tm.api;

import feign.Feign;
import feign.Headers;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface RestServiceApi {

    static RestServiceApi restServiceApi(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder().contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(RestServiceApi.class, baseUrl);
    }

    @GetMapping(value="/projects", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<Project> getProjects();

    @GetMapping(value="/projects/{projectId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Project getProject(@PathVariable("projectId") final String projectId);

    @GetMapping(value="/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<Task> getTasks();

    @GetMapping(value="/tasks/{taskId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Task getTask(@PathVariable("taskId") final String taskId);

    @PostMapping(value="/projects/merge", headers = MediaType.APPLICATION_JSON_VALUE)
    Project mergeProject(@RequestBody final Project project);

    @DeleteMapping(value="/projects/delete/{projectId}")
    void deleteProject(@PathVariable("projectId") final String projectId);

    @PostMapping(value="/tasks/merge")
    @Headers("Content-Type: application/json")
    Task mergeTask(@RequestBody final Task task);

    @DeleteMapping(value="/tasks/delete/{taskId}")
    void deleteTask(@PathVariable("taskId") final String taskId);

}
